#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function



####################################################################################
#
# sds011_liveplot.py                                               Version: 1.0 beta
#
# This is a program for the laser particulate matter sensor SDS011 from Nova Fitness
#
####################################################################################
# Tested with
# -----------
# - Windows 7, MacOS ???, Ubuntu 16.04 LTS, Linux Mint 18.3, Raspbian 2017-11-29
#       (installation instructions see below)
# - python 2.7
# - pyserial version 2.x and 3.x
# - Raspberry Pi 3 Model B (5V/2A power bank and power supply)
# - SDS011 firmware version 17-4-18 (year-month-day)
#
####################################################################################
# Instructions
# ------------
#       Follow installation instructions below and
#       SET DEVICEPATH OF SERIAL CONNECTION TO SDS011 below in this program
#
####################################################################################
# Program features
# ----------------
# - Measurement interval can be changed while recording is running
# - Timeline of plot can be changed while recording is running
# - Displays firmware version and device id
# - Automatic adaption of y-axis scale to highest value in actual viewport
# - Can write measurement values to csv files
# - Drives sensor to sleep mode if selected interval is greater than sensor warmup time  
# - Works also if only serial reading is possible but no writing (then without sleep mode)
#
# Features which can be configured by setting program variables:
# - Sensor warmup time (after wake from sleep mode) configurable
# - Optional write measurement values to csv files
# - Folder name for measurement data logging files configurable
# - Optional show saved filename if recording stopped
# - Window size and position configurable (also for low resolution displays down to 480x320)
# - Default interval and default timeline maximum at program start configurable
# - Line colors configurable
# - Sensor overflow values for pm25 and pm10 and overflow color configurable
# - Optional plot with bright or dark background (less power consumption on mobile use)
# - Optional show or hide window title bar
# - Step width of interval change configurable
# - Step width of timeline change configurable
# - Step width of automatic y-axis change configurable
# - Maximum number of plotted measurement values configurable (ring buffer)
# - Optional start recording at program start
# - Optional suppress warmup at program startup (not recommended)
# - Optional set sensor to sleep mode if quit program
# - Optional confirm dialog if quit program
# - Optional shutdown if quit program (works only on linux)
# - Optional confirm dialog before shutdown (works only on linux)
#
####################################################################################
# Explanation of terms
# --------------------
# sleep mode:   - sensor does not send any data and is waiting for commands
#               - fan is not working
#               - sensor electronic (laser) is not working
#               - sleep current < 4 mA
#               - reduced wear (according to SDS011 sensor specification service live is only up to 8000 hours)
# warmup time:  - time between waking up from sleep mode and measured values from sensor are stable
#               - measured values are not stable
# work mode:    - (wake mode) sensor sends measured values every second actively 
#
####################################################################################
# Respected documents
# -------------------
# - "Laser Dust Sensor Control Protocol V1.3" for SDS011
# - "Laser PM2.5 Sensor Specification V1.3" for SDS011
#
####################################################################################
# Remarks to sensor behaviour with firmware 17-4-18
# -------------------------------------------------
# - sending a sleep command when sensor is sleeping wakes sensor again
# - sending a get-sleep-mode command when sensor is sleeping wakes sensor too
#
####################################################################################
# Remarks to some USB-serial adapters
# -----------------------------------
#
# CH340 chip based adapters
# -------------------------
# The used sensor was delivered together with a USB-serial adapter which had a CH340 chip.
# This adapter could only read from serial port. Writing was not possible under all tested operating systems.
# Under Linux only the automatic loaded kernel module was tested.
# The CH340 adapter had the PCB lettering SDS011_USB2TTL_004.
#
# For installation of the official CH340 driver look at sparks.gogo.co.nz/ch340.html
# 
# Anyway with Raspberry Pi the cable of the delivered adapter was useful to connect the sensor
# directly to the GPIO port without use of the adapter (see installation instructions below).
#
# PL2303 chip based adapters (Prolific)
# -------------------------------------
# - Worked as expected under all tested operating systems.
# - Tested adapter: Adafruit 954 (has single wires at the serial end which fit to the sensor connector)
#
# Prolific USB to serial
# with PL2303 chip            SDS011 connector
# (colors Adafruit 954)
#                             1   NC   (edge pin)
#                             2   pm25
# +5V (red)           -----   3   +5V
#                             4   pm10
# GND (black)         -----   5   GND
# TX  (green)         ---->   6   RXD
# RX  (white)         <----   7   TXD
#
####################################################################################




################################################################################################################
# Installation instructions for python 2.7, pyserial, matplotlib and USB-serial adapter driver
################################################################################################################
# Windows installation (tested with Windows 7) #
################################################
#   1) Install Python
#       1a) Go to https://www.python.org/downloads/windows/ and download latest 2.7.xx version and install it
#    	1b) Add Python\ and Python\Scripts to $PATH
#           (detailed instructions at https://superuser.com/questions/143119
#           add not only C:\Python27 but also C:\Python27\Scripts assuming installation in C:\Python27\ )
#   2) Install pip
#       2a) Go to https://pip.pypa.io/en/stable/installing/
#       2b) Download get-pip.py
#       2c) Execute "python get-pip.py"
#   3) Install pyserial and matplotlib using pip
#       3a) Execute "pip install pyserial matplotlib"
#   4) Install driver for USB-serial adapter
#       4a) Try if adapter is recognized automatical by operating system and free from error
#           otherwise download and install driver
#           for CH340 based adapter at: http://www.wch.cn/download/CH341SER_EXE.html (chinese language)
#               (details at https://roboindia.com/tutorials/CH340-usb-to-serial-driver-installation )
#           for PL2303 based adapter at: http://www.prolific.com.tw/US/ShowProduct.aspx?p_id=225&pcid=41
#           for other adapters look at manufacturer website
#   5) Adjust devicepath entry below in this program
#       5a) Look in windows device manager to see number of installed com port
#           and use number to adjust the devicepath entry below in this program (e.g. COM3)
################################################################################################################
# Macintosh installation (tested with MacOS 10.XX ???) #
####################################################
#   1) Python is already installed by default
#   2) Install pip
#       2a) Execute "sudo easy_install pip"
#   3) Install pyserial and matplotlib using pip
#       3a) Execute "pip install pyserial matplotlib"
#   4) Install driver for USB-serial adapter
#       4a) Try if adapter is recognized automatical by operating system and free from error
#           otherwise download and install driver
#           for CH340 based adapter at: http://www.wch.cn/download/CH341SER_MAC_ZIP.html
#           for PL2303 based adapter at: http://www.prolific.com.tw/US/ShowProduct.aspx?p_id=229&pcid=41
#               (details at https://plugable.com/2011/07/12/installing-a-usb-serial-adapter-on-mac-os-x )
#           for other adapters look at manufacturer website
#   5) Adjust devicepath entry below in this program
#       5a) Execute "ls /dev/ | grep usbserial" to see usbserial devices
#           and use device name to adjust the devicepath entry below in this program (choose tty device ???)
################################################################################################################
# Ubuntu 16.04.03 LTS (tested with live CD) #
#############################################
#   1) Python is already installed by default
#   2) Add repository multiverse (if it is not already in repository list)
#       2a) Execute "sudo apt-add-repository multiverse"
#   3) Install pyserial and matplotlib
#       3a) Execute "sudo apt-get update"
#       3b) Execute "sudo apt-get install python-serial python-matplotlib"
#   4) Add actual user to dialout group
#       4a) Execute "sudo gpasswd --add ${USER} dialout"
#       4b) log out and log in again for it to be effective
#   5) Adjust devicepath entry below in this program
################################################################################################################
# Linux Mint 18.3 (based on Ubuntu 16.04 LTS) (tested with live CD) #
#####################################################################
#   1) Python is already installed by default
#   2) Install pyserial and matplotlib
#       2a) Execute "sudo apt-get update"
#       2b) On Linux Mint 18.3 pyserial is installed by default (otherwise "sudo apt-get install python-serial")
#       2c) Execute "sudo apt-get install python-matplotlib"
#   3) Install Tkinter
#       3a) Execute "sudo apt-get install python-tk"
#   4) Add actual user to dialout group
#       4a) Execute "sudo gpasswd --add ${USER} dialout"
#       4b) log out and log in again for it to be effective
#   5) Adjust devicepath entry below in this program
################################################################################################################
# Raspbian 2017-11-29 on Raspberry Pi 3 Model B - With USB-serial adapter #
###########################################################################
#   1) Python is already installed by default
#   2) Install pyserial and matplotlib
#       2a) Execute "sudo apt-get update"
#       2b) On Raspbian 2017-11-29 pyserial is installed by default (otherwise "sudo apt-get install python-serial")
#       2c) Execute "sudo apt-get install python-matplotlib"
#   3) Adjust devicepath entry below in this program
################################################################################################################
# Raspbian 2017-11-29 on Raspberry Pi 3 Model B - With GPIO connector #
#######################################################################
#   1) Python is already installed by default
#   2) Install pyserial and matplotlib
#       2a) Execute "sudo apt-get update"
#       2b) On Raspbian 2017-11-29 pyserial is installed by default (otherwise "sudo apt-get install python-serial")
#       2c) Execute "sudo apt-get install python-matplotlib"
#   3) Enable serial port on GPIO and disable login shell over serial
#       3a) Execute "sudo raspi-config" -> Choose Interfacing Options -> Choose Serial
#           -> Choose No (no login shell over serial) -> Choose Yes (enable serial port) -> Ok -> Finish -> Reboot
#   4) Adjust devicepath entry below in this program
#   5) Connect SDS011 to the GPIO port (power off before)
#       5a) If the often together with the SDS011 sensor delivered USB-serial adapter is available
#           (CH340 chip based with PCB lettering SDS011_USB2TTL_004) the connection is very easy.
#           Just unplug the cable from the adapter and plug it on the GPIO port.
#           The connector pin where no wire is connected has to be placed on the GPIO pin at the outer edge.
#
# Raspberry Pi 3             SDS011<->CH340 cable connector     SDS011 connector
# GPIO connector             on side of CH340 adapter           
# pin                                                           pin
#  2  +5V (edge pin)          NC (no connected wire)
#  4  +5V            -----   +5V                         -----   3   +5V
#  6  GND            -----   GND                         -----   5   GND
#  8  TXD            ---->   RXD                         ---->   6   RXD
# 10  RXD            <----   TXD                         <----   7   TXD
################################################################################################################
# Raspbian 2017-11-29 - Autostart Tkinter Python scripts #
##########################################################
# (Tkinter based programs can not be started by rc.local)
# Edit: /home/pi/.config/lxsession/LXDE-pi/autostart
# Add: @/usr/bin/python/ /home/pi/sds011_liveplot.py
################################################################################################################


import serial, struct, pylab
from Tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import style
import tkMessageBox
import time, datetime
import string, bisect
import os

global pm_25, pm_10     # measured values pm25, pm10 set by serial_read()
pm_25 = 0.0
pm_10 = 0.0
global sds011



######################################################
# SET DEVICEPATH OF SERIAL CONNECTION TO SDS011 HERE #
######################################################
#sds011 = "COMxx"              # Windows (e.g. "COM3")

#sds011 = "/dev/tty.usbserial" # Macintosh ??? (oder "/dev/cu.usbserial" ???)

sds011 = "/dev/ttyUSB0"       # Ubuntu, Linux Mint, Raspberry Pi (all with PL2303 or CH340 USB-serial adapter)

#sds011 = "/dev/serial0"       # Raspberry Pi with GPIO (also usable "/dev/ttyS0")



#############################
# SET DIVERSE FEATURES HERE #
#############################
interval_default = 1    # default measure interval in seconds at program start
timeline_default = 300  # default timeline maximum in seconds at program start
yaxis_default = 1000    # default y-axis maximum in μg/m^3 at program start

# steps for interval +/-
intvl_list = [ 1, 2, 5, 10, 15, 20, 30, 45, 60, 90, 120, 180, 240, 300, 600, 900, 1200, 1800 ]
# steps for timeline +/-
xmax_list = [ 30, 60, 120, 180, 240, 300, 600, 1200, 1800, 2700, 3600, 7200, 14400, 28800, 57600, 86400 ]
# steps for y-axis maximum (automatic)
ymax_list = [ 2, 4, 8, 16, 25, 50, 75, 100, 200, 300, 400, 500, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200 ]

window_xsize = 480      # width of program window
window_ysize = 320      # height of program window
window_xpos = 0         # horizontal position of program window 
window_ypos = 0         # vertical position of program window
dark_background = 1     # plot background (0: normal background, 1: dark background)
hide_decoration = 0     # show or hide title bar (0: show, 1: hide)

                               # color names: matplotlib.org/examples/color/named_colors.html
pm25_color =  "tomato"         # color for pm25 plot (e.g. "red", "orangered", "tomato)
pm10_color = "deepskyblue"     # color for pm10 plot (e.g. "blue", "royalblue", "deepskyblue", "turquoise")
overflow_color = "lightsalmon" # show pm25 and pm10 values with other color if sensor overflow occurs
pm25_overflow = 999.9   # show pm25 value with overflow_color if overflow reached
pm10_overflow = 2200.0  # show pm10 value with overflow_color if overflow reached
                        # according to sensor specification measurement range is 0.0-999.9 μg/m^3 for pm25 and pm10
                        # maximum values received from sensor are 999.9 for pm25 and 2200.0 for pm10
maxpoints_default = 86400 # max number of plotted points kept in ring buffer per plotted line
                          # e.g. 86400 for 1day/interval 1s or 5days/interval 5s 

log_foldername = "_logfiles_sds011"     # folder name for measurement data logging files
write_logfiles = 1      # write measurement values to csv files (0: no files, 1: write files)
show_filename = 1       # show saved filename if recording stopped (0: do not show, 1: show)
autostart = 0           # autostart (0: start recording manually, 1: start recording at program start)
show_quit_dialog = 1    # show confirm dialog if quit program (0: no dialog, 1: show dialog) 
shutdown_if_quit = 0    # try to shutdown if quit program (works only on linux) (0: no shutdown, 1: shutdown)
show_shutdown_dialog= 0 # show confirm dialog before shutdown (works only on linux) (0: no dialog, 1: show dialog)
        

# next settings only if serial write to sensor is possible:
warmup_time = 30        # "The data is stable when the sensor works after 30 seconds"
                        # see documentation: "Laser Dust Sensor Control Protocol V1.3"
sleep_if_quit = 0       # set sensor to sleep mode if quit program (0: wake if quit, 1: sleep if quit)
warmup_at_startup = 1   # warmup at program startup (0: no warmup, 1: warmup)
                        # set only to 0 if sure that sensor is working warmup_time before program start


###############################################################################
# serial sensor handling functions                                            #
# - functions implement most of "Laser Dust Sensor Control Protocol V1.3"     #
# - code is modified from codegists.com/snippet/python/sds011_kadamski_python #
###############################################################################
SERIAL_DEBUG = 0

def dump(d, prefix=''):
    print(prefix + ' '.join(x.encode('hex') for x in d))

def construct_cmd(cmd, data=[]):
    assert len(data) <= 12
    data += [0,]*(12-len(data))
    checksum = (sum(data)+cmd-2)%256
    ret = "\xaa\xb4" + chr(cmd)
    ret += ''.join(chr(x) for x in data)
    ret += "\xff\xff" + chr(checksum) + "\xab"
    ser.flushInput()
    if SERIAL_DEBUG:
        dump(ret, '> ')
    return ret

def read_response():
    byte = ""
    i = 0
    while byte != "\xaa" and i<3:
        byte = ser.read(size=1)
        i += 1
    d = ser.read(size=9)
    if SERIAL_DEBUG:
        dump(d, '< ')
    return byte + d

def process_data(d):
    r = struct.unpack('<HHxxBB', d[2:])
    pm25 = r[0]/10.0
    pm10 = r[1]/10.0
    checksum = sum(ord(v) for v in d[2:8])%256
    print("PM 2.5: {} μg/m^3  PM 10: {} μg/m^3 CRC={}".format(pm25, pm10, "OK" if (checksum==r[2] and r[3]==0xab) else "NOK"))

# 4) Set sleep and work
def sds_set_sleep_mode(mode):           # mode 0: sleep, 1: work
    ser.write(construct_cmd(6, [1, mode]))
    d = read_response()
    return d

# 1) Set data reporting mode
def sds_set_data_reporting_mode(mode):  # mode 0: active mode, 1: query mode
    ser.write(construct_cmd(2, [1, mode]))
    d = read_response()
    return d

# 2) Query data command
def sds_query_data():
    ser.write(construct_cmd(4))
    d = read_response()
    if d[1] == "\xc0":
        process_data(d)

# 5) Set working period
def sds_set_working_period(period):     # period 0: continuous (default), 1-30 minutes: work 30 s and sleep n*60-30 s
    ser.write(construct_cmd(8, [1, period]))
    read_response()

# 6) Check firmware version
def sds_firmware_version():
    ser.write(construct_cmd(7))
    d = read_response()
    r = struct.unpack('<BBBHBB', d[3:])
    checksum = sum(ord(v) for v in d[2:8])%256
    #print("Year: {}, Month: {}, Day: {}, ID: {}, CRC={}".format(r[0], r[1], r[2], hex(r[3]), "OK" if (checksum==r[4] and r[5]==0xab) else "NOK"))
    return str(r[0]) + "-" + str(r[1]) + "-" + str(r[2])

# 3) Set device id
def sds_set_device_id(id):
    id_h = (id>>8) % 256
    id_l = id % 256
    ser.write(construct_cmd(5, [0]*10+[id_l, id_h]))
    read_response()

def demo_serial_sensor_handling():   # not used in this program
    print ("===== Wakeup =====")
    sds_set_sleep_mode(1)
    time.sleep(2)                       
    print ("===== Set data reporting mode: QUERY MODE =====")
    sds_set_data_reporting_mode(1)
    print ("===== Query data =====")
    sds_query_data()
    print ("===== Set data reporting mode: ACTIVE MODE =====")
    sds_set_data_reporting_mode(0)
    print ("===== Check firmware version =====")
    print ("Firmware version (year-month-day): " + sds_firmware_version())
    print ("===== Sleep =====")
    sds_set_sleep_mode(0)
    


###############################################################################
# Tkinter application                                                         #
###############################################################################
class App():
    def __init__(self, master):
        global ser
        global sds011
        
        self.live_interval = interval_default   # plot interval in seconds
        self.live_xmax = timeline_default       # x-axis maximum in seconds
        self.live_ymax = yaxis_default          # y-axis maximum in μg/m^3
        self.live_maxpoints = maxpoints_default # max number of plotted points kept in ring memory per plotted line
                                                # e.g. 86400 for 1day/interval 1s or 5days/interval 5s 

        self.live_run = False   # live plotting stopped
        self.live_x = []        # x values (time)
        self.live_y1 = []       # y values PM25
        self.live_y2 = []       # y values PM10

        self.warmup_time = warmup_time  # warmup time
        self.warmup_cntr = 0        # data stable if counter is 0
        self.sleep_cntr = 0         # counter for sleep mode
        self.work_cntr = 0          # counter for sleep mode
        self.sensor_sleep_mode = 1  # 0: sleep, 1: work
        self.sensor_rw = 0          # 0: only read ok, 1: read and write ok

        self.live_plotjob_id = None   # id of after-job which calls sensor_live_do
        self.live_wakejob_id = None   # id of after-job which calls job to wake sensor
        self.serial_readjob_id = None # id of after-job which calls serial_read
        self.starttime_after = None   # time of after-job which calls sensor_live_do

        self.master = master
        frame = Frame(master)
        frame.pack()

        frame.columnconfigure(1, minsize=78)
        
        Label(frame, text="PM 2.5: ",     font=(None, 11)).grid(row=0, sticky="e")
        Label(frame, text=u" µg/m\u00b3", font=(None, 11)).grid(row=0, column=2, sticky="w")
        Label(frame, text="PM  10: ",     font=(None, 11)).grid(row=1, sticky="e")
        Label(frame, text=u" µg/m\u00b3", font=(None, 11)).grid(row=1, column=2, sticky="w")
        
        self.result_pm25 = DoubleVar()
        self.label_result_pm25 = Label(frame, textvariable=self.result_pm25, font=(None, 15), bg="white")
        self.label_result_pm25.grid(row=0, column=1, sticky="e")
        
        self.result_pm10 = DoubleVar()
        self.label_result_pm10 = Label(frame, textvariable=self.result_pm10, font=(None, 15), bg="white")
        self.label_result_pm10.grid(row=1, column=1, sticky="e")
        
        self.sleep_state = StringVar()
        self.label_sleep_state = Label(frame, textvariable=self.sleep_state, fg="black", bg="white")
        self.label_sleep_state.grid(row=0, column=5)

        self.actual_time = StringVar()
        Label(frame, textvariable=self.actual_time, fg="black", bg="white").grid(row=0, column=6)

        self.button1 = Button(frame, text="Start\nRecord", font=(None, 11), borderwidth=2, command=self.sensor_live)
        self.button1.grid(row=0, rowspan=2, column=3, columnspan=2, sticky="nsw")

        button2 = Button(frame, text="Info", command=self.sensor_info)
        button2.grid(row=1, column=5, padx=13)

        button3 = Button(frame, text="Quit", command=self.quit)
        button3.grid(row=1, column=6)

        button5 = Button(frame, text="Interval -", width=6, command=lambda: self.set_interval(-1))
        button5.grid(row=2, column=0, sticky="w")

        self.interval_disp = StringVar()
        Label(frame, textvariable=self.interval_disp, font=(None, 12), bg="white").grid(row=2, column=1)
        self.interval_disp.set(str(self.live_interval) + " s")
        
        button6 = Button(frame, text="Interval +", width=6, command=lambda: self.set_interval(1))
        button6.grid(row=2, column=2)

        Label(frame, text=" ").grid(row=2, column=3)

        button7 = Button(frame, text="Timeline -", width=6, command=lambda: self.set_xmax(-1))
        button7.grid(row=2, column=4)

        self.xmax_disp = StringVar()
        Label(frame, textvariable=self.xmax_disp, font=(None, 12), bg="white").grid(row=2, column=5)
        self.xmax_disp.set(str(self.live_xmax) + " s")
        
        button8 = Button(frame, text="Timeline +", width=6, command=lambda: self.set_xmax(1))
        button8.grid(row=2, column=6)

        if dark_background:
            style.use("dark_background")
        
        fig = pylab.Figure()
        self.canvas = FigureCanvasTkAgg(fig, master=master)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

        self.ax = fig.add_subplot(111)
        self.ax.grid(True)
        self.ax.set_title("PM2.5 and PM10", fontsize=10)
        self.ax.set_xlabel("Time (seconds)")
        self.ax.set_ylabel(u"PM (µg/m\u00b3)")
        self.ax.axis([0, self.live_xmax, 0, self.live_ymax])

        self.update_clock() # start clock


        # initialize serial interface
        try:
            ser = serial.Serial(sds011, baudrate=9600, stopbits=1, parity=serial.PARITY_NONE, timeout=3)
        except Exception, e:
            tkMessageBox.showerror("Error", "\n Serial interface " + sds011 + " not available. \n\n" + str(e))
            root.destroy()
            return
        try:
            ser.flushInput()
        except Exception, e:
            tkMessageBox.showerror("Error", "\n Serial error at flushInput. \n\n" + str(e))


        # determine if serial interface is only readable or readable and writeable
        
        # if response is not correct assume writing not ok (sleep mode not possible)
        response = sds_set_sleep_mode(1)    # send wake command (also to check the response)
        if response[0:5].encode('hex') == "aac5060101":
            self.sensor_rw = 1  # set self.sensor_rw 
            if warmup_at_startup:
                self.warmup_cntr = self.warmup_time
            sds_set_data_reporting_mode(0)   # set sensor to active mode (mostly unnecessary)
        #     print("START - Writing OK:   ") # DEBUG
        # else:                               # DEBUG
        #     print("START - Writing NOT OK") # DEBUG

        self.serial_read()  # start serial reading measurement values

        # if values set by serial_read() == 0.0  assume reading not ok
        if not (pm_25 and pm_10):   
            # print("START - Reading NOT OK") # DEBUG
            self.master.after_cancel(self.serial_readjob_id)  # cancel after-job which calls serial_read
            tkMessageBox.showerror("Error", "\n Serial interface " + sds011 + " present but no valid data received. \n")
            root.destroy()
            return

        # print ("START - Reading OK:   ", pm_25, " ", pm_10, "   ", datetime.datetime.now().strftime("%Y%m%d;%H:%M:%S.%f")[:-3] ) # DEBUG        

        if autostart:
            self.sensor_live()


    # increase/decrease interval
    def set_interval(self, inp):
        intvl_index = intvl_list.index(self.live_interval)
        if inp == -1 and intvl_index>0:
            self.live_interval = intvl_list[intvl_index-1]
        elif inp == 1 and intvl_index<len(intvl_list)-1:
            self.live_interval = intvl_list[intvl_index+1]
        else:
            return
        self.interval_disp.set(str(self.live_interval) + " s")

        # cancel pending after-jobs
        if self.live_wakejob_id is not None:
            # print("### SET INTERVAL ####### self.live_wakejob_id: ", self.live_wakejob_id)    # DEBUG
            self.master.after_cancel(self.live_wakejob_id)  # cancel after-job which calls job to wake sensor
            self.live_wakejob_id = None
        if self.live_plotjob_id is not None:
            # print("### SET INTERVAL ####### self.live_plotjob_id: ", self.live_plotjob_id)    # DEBUG
            self.master.after_cancel(self.live_plotjob_id)  # cancel after-job which calls sensor_live_do
            self.live_plotjob_id = None

            if self.sensor_sleep_mode == 0:
                time.sleep(.03) # if quickly pressed interval+/- serial can crash (better wait between sleep and wake)
                self.sensor_wake()

            self.sensor_live_do()   # call sensor_live_do now                      


    # increase/decrease x-axis maximum
    def set_xmax(self, inp):
        xmax_index = xmax_list.index(self.live_xmax)
        if inp == -1 and xmax_index>0:
            self.live_xmax = xmax_list[xmax_index-1]
        elif inp == 1 and xmax_index<len(xmax_list)-1:
            self.live_xmax = xmax_list[xmax_index+1]
        else:
            return
        self.xmax_disp.set(str(self.live_xmax) + " s")

        # automatic y-axis
        xindex = bisect.bisect_right(self.live_x, self.live_xmax)
        ytop = max(max(self.live_y1[:xindex]), max(self.live_y2[:xindex]))
        self.live_ymax = min([i for i in ymax_list if i >= ytop])
        self.ax.axis([0, self.live_xmax, 0, self.live_ymax])
        self.canvas.draw()


    # sensor wake
    def sensor_wake(self):
        if self.sensor_sleep_mode == 1:
            return
        sds_set_sleep_mode(1)
        self.sensor_sleep_mode = 1
        # print (">>>WAKE>>> sensor_wake: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])    # DEBUG

        if self.sensor_rw:
            self.warmup_cntr = self.warmup_time

    # sensor sleep
    def sensor_sleep(self):
        if self.sensor_sleep_mode == 0:     # sending sleep command while sleep mode activates work mode again
            return                          # therefore return if called while sleep mode
        sds_set_sleep_mode(0)
        self.sensor_sleep_mode = 0
        # print (">>>SLEEP>>> sensor_sleep: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])  # DEBUG

        if self.sensor_rw:
            self.warmup_cntr = 0


    # display sensor information
    def sensor_info(self):
        if self.sensor_rw == 1:
            response = sds_set_data_reporting_mode(0)      # call only to evaluate the response
            device_id = ' '.join(x.encode('hex') for x in response[6:8])
            inf = "Firmware version (year-month-day):\n   " + sds_firmware_version() + "\n\nDevice ID (byte1 byte2):\n   " + device_id
            tkMessageBox.showinfo("Sensor Info", inf)
        else:
            tkMessageBox.showinfo("Sensor Info", "Can't get sensor information because writing to sensor is not possible")


    # write values to display and return values (not used in this program)
    def sensor_read(self):
        global pm_25
        global pm_10
        self.result_pm25.set(pm_25)
        self.result_pm10.set(pm_10)
        data = [pm_25, pm_10]
        return data
        

    # every second read values from serial interface
    def serial_read(self):
        global ser
        global pm_25
        global pm_10

        self.serial_readjob_id = root.after(1000, self.serial_read) # run itself again after xxxx ms

        # print ("serial_read(): " + datetime.datetime.now().strftime('%H:%M:%S.%f')[:-3])  # DEBUG

        if self.sensor_sleep_mode==0:
            return

        try:
            ser.flushInput()
        except Exception, e:
            tkMessageBox.showerror("Error", "\n Serial error at flushInput. \n\n" + str(e))
        
        try:
            firstbyte  = ser.read(size=1)
        except Exception, e:
            tkMessageBox.showerror("Error", "\n Failure reading serial interface. \n\n" + str(e))

        secondbyte = ser.read(size=1)

        if firstbyte == "\xAA" and secondbyte == "\xC0":
            try:
                sentence = ser.read(size=8) 
                readings = struct.unpack('<hhxxcc',sentence) 
            except Exception, e:
                tkMessageBox.showerror("Error", "\n Failure reading serial interface. \n\n" + str(e))
                    
            # print ( "   ", pm_25, " ", pm_10, "   ", datetime.datetime.now().strftime("%Y%m%d;%H:%M:%S.%f")[:-3] )  # DEBUG

            pm_25 = round(readings[0]/10.0, 3)
            pm_10 = round(readings[1]/10.0, 3)	  

            self.result_pm25.set(pm_25)
            self.result_pm10.set(pm_10)
            self.label_result_pm25.configure(bg= "white" if pm_25 < pm25_overflow else overflow_color)
            self.label_result_pm10.configure(bg= "white" if pm_10 < pm10_overflow else overflow_color)

        
    # start/stop liveplot
    def sensor_live(self):
        global fname_csv
        if self.live_run == False:
            self.live_run = True
            self.button1.configure(text="Stop\nRecord", bg="gray75", relief=SUNKEN)
            self.live_x = []
            self.live_y1 = []
            self.live_y2 = []
            program_dir = os.path.dirname(os.path.realpath(__file__))
            log_path = os.path.join(program_dir, log_foldername)
            if write_logfiles:
                if not os.path.exists(log_path):
                    os.makedirs(log_path)
            fname_csv = os.path.join(log_path, 'particulate_matter_'+datetime.datetime.now().strftime("%Y%m%d_%H_%M_%S")+'.csv')
            self.sensor_live_do()
        else:
            self.live_run = False
            self.button1.configure(text="Start\nRecord", bg="gray85", relief=RAISED)
            # cancel pending after-jobs
            if self.live_wakejob_id is not None:
                self.master.after_cancel(self.live_wakejob_id)  # cancel after-job which calls job to wake sensor
                self.live_wakejob_id = None
            if self.live_plotjob_id is not None:
                self.master.after_cancel(self.live_plotjob_id)  # cancel after-job which calls sensor_live_do
                self.live_plotjob_id = None

            if self.sensor_sleep_mode == 0:
                self.sensor_wake()

            if show_filename:
                tkMessageBox.showinfo("Filename", "Record filename:\n\n" + fname_csv)
 

    # liveplot job
    def sensor_live_do(self):
        global pm_25
        global pm_10
        global fname_csv

        if self.live_run == False:
            return

        if self.warmup_cntr > 0:     # reschedule execution until end of warmup
            self.live_plotjob_id = root.after(1000, self.sensor_live_do) # run itself again after 1 s
            return

        self.live_plotjob_id = root.after(self.live_interval*1000, self.sensor_live_do) # run itself again after xxxx ms
        # print(">>>>>>> sensor_live_do /live_plotjob_id: ", self.master.call('after', 'info', self.live_plotjob_id))       # DEBUG

        # if interval > warmup_time:  set sensor to sleep mode
        #                        and  start job to wake sensor (warmup_time before next sensor_live_do job)
        if self.live_interval > self.warmup_time and self.sensor_rw:
            self.sensor_sleep()
            self.sleep_cntr = self.live_interval - self.warmup_time
            self.live_wakejob_id = root.after((self.live_interval-self.warmup_time)*1000, self.sensor_wake) # run sensor_wake after xxxx ms
            # print(">>>>>>> sensor_live_do /live_wakejob_id: ", self.master.call('after', 'info', self.live_wakejob_id))   # DEBUG
        else:
            self.work_cntr = self.live_interval

        time_since_last_call = int(time.time() - self.starttime_after) if self.starttime_after is not None else 0
        self.starttime_after = time.time()
        if time_since_last_call == 0 and self.live_x:
            return

        pm25 = pm_25
        pm10 = pm_10
        # print (">>> sensor_live_do: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])    # DEBUG

        if write_logfiles:
            self.write_csv(str(pm25), str(pm10), datetime.datetime.now().strftime("%Y%m%d;%H:%M:%S"), fname_csv)

        if self.live_x:
            self.live_x = [ i + time_since_last_call for i in self.live_x ] # add to all elements

        self.live_x.insert(0, 0)        # insert 0 as first
        self.live_y1.insert(0, pm25)   # insert as first
        self.live_y2.insert(0, pm10)   # insert as first
        if len(self.live_x) > self.live_maxpoints :
            del(self.live_x[-1])        # delete last
            del(self.live_y1[-1])       # delete last
            del(self.live_y2[-1])       # delete last

        # automatic y-axis
        xindex = bisect.bisect_right(self.live_x, self.live_xmax)
        ytop = max(max(self.live_y1[:xindex]), max(self.live_y2[:xindex]))
        self.live_ymax = min([i for i in ymax_list if i >= ytop])
        self.ax.axis([0, self.live_xmax, 0, self.live_ymax])

        self.ax.lines = []	        # clear plot lines
        line1, = self.ax.plot(self.live_x, self.live_y1, pm25_color, marker="o", markersize=3)	# marker list: matplotlib.org/api/markers_api.html
        line2, = self.ax.plot(self.live_x, self.live_y2, pm10_color, marker="^", markersize=3)

        self.canvas.draw()



    # write measurement values to file
    def write_csv(self, pm25, pm10, value_time, value_fname):
        pm25 = string.replace(pm25, ".", ",")
        pm10 = string.replace(pm10, ".", ",")
        with open(value_fname,'a') as file:
            line = "" + pm25 + ";" + pm10 + ";" + value_time
            file.write(line)
            file.write('\n')
            file.close()


    # every second (update GUI, warmup handling)
    def update_clock(self):
        root.after(1000, self.update_clock) # run itself again after 1 s
        now = time.strftime("%H:%M:%S")
        self.actual_time.set(now)

        # update sleep/warmup/work status in display
        if self.sensor_sleep_mode == 0:
            self.label_sleep_state.configure(fg="white", bg="black")     # sleep colors
            self.sleep_state.set("Sleep " + str(self.sleep_cntr))
            self.label_result_pm25.configure(fg="gray42")
            self.label_result_pm10.configure(fg="gray42")
            self.sleep_cntr -= 1    # decrement sleep_cntr
        elif self.warmup_cntr > 0:    
            self.label_sleep_state.configure(fg="black", bg="peachpuff") # warmup colors  # orange, lightsalmon, lightpink
            self.sleep_state.set("Warmup " + str(self.warmup_cntr))
            self.label_result_pm25.configure(fg="gray42")
            self.label_result_pm10.configure(fg="gray42")
            self.warmup_cntr -= 1   # decrement warmup_cntr
        else:
            self.label_sleep_state.configure(fg="black", bg="white")     # wake colors
            if self.live_interval<10 or self.live_run == False:    
                self.sleep_state.set("Work")
            else:
                self.sleep_state.set("Work " + str(self.work_cntr))
            self.label_result_pm25.configure(fg="black")
            self.label_result_pm10.configure(fg="black")
            self.work_cntr -= 1   # decrement work_cntr

        # print("************AFTER INFO: ", self.master.call('after', 'info')   # DEBUG
        

    # quit
    def quit(self):
        if show_quit_dialog:
            if not tkMessageBox.askokcancel("Quit", "Do you really want to quit?"):
                return
        if self.sensor_rw:
            self.sensor_sleep() if sleep_if_quit else self.sensor_wake()
        if show_shutdown_dialog:
            if not tkMessageBox.askyesno("Shutdown", "Do you really want to shutdown?", default="no"):
                root.destroy()
                return
        root.destroy()
        if shutdown_if_quit:
            if os.uname()[0] == 'Linux':
                os.system("sudo shutdown -h now")



# start tkinter applicatiom
root = Tk()
root.wm_title("Particulate Matter Sensor SDS011")
if hide_decoration:
    root.overrideredirect(True)   # hide window decoration
root.geometry(str(window_xsize)+"x"+str(window_ysize)+"+"+str(window_xpos)+"+"+str(window_ypos))
app = App(root)
root.mainloop()
