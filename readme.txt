SDS011 liveplot is written in python 2 by a Freiburg programmer who wants to stay anonymous. He asked me to publish the code and promote the programm to give more people the ability to collect air quality data. 

Before using the programm please read the code. We will be putting some more information up on the wiki here as well


The programm has been tested with

Windows 7, Ubuntu 16.04 LTS, Linux Mint 18.3, Raspbian 2017-11-29
python 2.7
pyserial version 2.x and 3.x
Raspberry Pi 3 Model B (5V/2A power bank and power supply)
SDS011 firmware version 17-4-18 (year-month-day)


We will be testing with a MacOs soon and report our results.


What do I need?

For using this programm you will need a 


Nova SDS 011 particle matter sensor. Which you can get from AliExpress for about 18 EUR , if you wait about 4  - 6 weeks: https://de.aliexpress.com/wholesale?site=deu&SortType=price_asc&shipCountry=de&SearchText=sds011&CatId=523

A Raspberry Pi 3 Model B. That is the one we tested. You may alos use an older Pi or even Raspberry Pi 0 w. https://www.amazon.de/Raspberry-1373331-Pi-Modell-Mainboard/dp/B07BDR5PDW/ref=sr_1_5?s=computers&ie=UTF8&qid=1529672133&sr=1-5&keywords=raspberry+pi+3+b%2B 
For operating the Pi you will need a micro sd card, a power source and maybe a case. Go shop around what fits best for you. Some people use a cheap display https://www.amazon.de/dp/B01CNLYL1C/ref=cm_sw_em_r_mt_dp_U_SiplBbK6ZEKB7 320*480 Resolution with Touch Screen should be enough.


Depending on the way you want to connect your pi with the sensor you may have to either solder PINS to the GPIO's of the Pi 0w or use a mini USB to USB female Adapter to connect it to the sensor. This is not necessary with the more expensive PI 3.


Program features


Measurement interval can be changed while recording is running
Timeline of plot can be changed while recording is running
Displays firmware version and device id
Automatic adaption of y-axis scale to highest value in actual viewport
Can write measurement values to csv files
Drives sensor to sleep mode if selected interval is greater than sensor warmup time

Works also if only serial reading is possible but no writing (then without sleep mode)


Features which can be configured by setting program variables:


Sensor warmup time (after wake from sleep mode) configurable
Optional write measurement values to csv files
Folder name for measurement data logging files configurable
Optional show saved filename if recording stopped
Window size and position configurable (also for low resolution displays down to 480x320)
Default interval and default timeline maximum at program start configurable
Line colors configurable
Sensor overflow values for pm25 and pm10 and overflow color configurable
Optional plot with bright or dark background (less power consumption on mobile use)
Optional show or hide window title bar
Step width of interval change configurable
Step width of timeline change configurable
Step width of automatic y-axis change configurable
Maximum number of plotted measurement values configurable (ring buffer)
Optional start recording at program start
Optional suppress warmup at program startup (not recommended)
Optional set sensor to sleep mode if quit program
Optional confirm dialog if quit program
Optional shutdown if quit program (works only on linux)
Optional confirm dialog before shutdown (works only on linux)



Explanation of terms


sleep mode:


sensor does not send any data and is waiting for commands
fan is not working
sensor electronic (laser) is not working
sleep current < 4 mA
reduced wear (according to SDS011 sensor specification service live is only up to 8000 hours)



warmup time:


time between waking up from sleep mode and measured values from sensor are stable
measured values are not stable



work mode:


(wake mode) sensor sends measured values every second actively 



Respected documents


"Laser Dust Sensor Control Protocol V1.3" for SDS011
"Laser PM2.5 Sensor Specification V1.3" for SDS011



Remarks to sensor behaviour with firmware 17-4-18


sending a sleep command when sensor is sleeping wakes sensor again
sending a get-sleep-mode command when sensor is sleeping wakes sensor too



Remarks to some USB-serial adapters


CH340 chip based adapters

The used sensor was delivered together with a USB-serial adapter which had a CH340 chip.
This adapter could only read from serial port. Writing was not possible under all tested operating systems.
Under Linux only the automatic loaded kernel module was tested.
The CH340 adapter had the PCB lettering SDS011_USB2TTL_004.

For installation of the official CH340 driver look at sparks.gogo.co.nz/ch340.html

Anyway with Raspberry Pi the cable of the delivered adapter was useful to connect the sensor
directly to the GPIO port without use of the adapter (see installation instructions below).


PL2303 chip based adapters (Prolific)


Worked as expected under all tested operating systems.
Tested adapter: Adafruit 954 (has single wires at the serial end which fit to the sensor connector)


Prolific USB to serial
 with PL2303 chip            SDS011 connector
 (colors Adafruit 954)
                             1   NC   (edge pin)
                             2   pm25
 +5V (red)           -----   3   +5V
                             4   pm10
 GND (black)         -----   5   GND
 TX  (green)         ---->   6   RXD
 RX  (white)         <----   7   TXD
 
 
 Various operating systems
 
 Before using it you have to configure the programm for your operating systems. 
 
 You find information about this in the Code: 
 
 ######################################################
# SET DEVICEPATH OF SERIAL CONNECTION TO SDS011 HERE #
######################################################
#sds011 = "COMxx"              # Windows (e.g. "COM3")

#sds011 = "/dev/tty.usbserial" # Macintosh ??? (oder "/dev/cu.usbserial" ???)

sds011 = "/dev/ttyUSB0"       # Ubuntu, Linux Mint, Raspberry Pi (all with PL2303 or CH340 USB-serial adapter)

#sds011 = "/dev/serial0"       # Raspberry Pi with GPIO (also usable "/dev/ttyS0")

if you use Windows you have to replace the xx with the ComPort: e.g. COM3
on the pi you can use two diffrent settings depending if you connect the device via GPIO Pins: then remove the # at the #sds011 = "/dev/serial0"       # Raspberry Pi with GPIO (also usable "/dev/ttyS0") or if you use it connected to USB do nothing.
Putting an # in front of the line will make it inactive.

You find installation instructions for your operating System in the Code. 

 